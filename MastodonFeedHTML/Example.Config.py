# Feeds configuration.
Feeds = [
	{
		"URLs": ["https://mastodon.social/@Mastodon", "https://botsin.space/@sitoctt"], # List of Mastodon profile URLs to scrape.
		"IncludeRetoots": True,
		"IncludeReplies": True,
		"LocalSave": True, # Whether or not to save posts as HTML files locally.
		"SendMail": False, # Whether or not to send posts via email.
		"MailTo": ["example@example.com"] # List of email recipients.
	}
]

# SMTP configuration (only required for sending mail).
MailUsername = "example@example.com" # Your full email address.
MailPassword = "Example"
MailServer = "smtp.example.com"
MailPort = 465 # (Usually) 465 for SSL, 587 for TLS, 25 for No encryption.
MailEncryption = "SSL" # SSL, TLS, or None.

# How often to refresh the feeds (in seconds). Set to 0 for a single run, instead of having the program sleep.
LoopTime = 300

# Additional time (in seconds) to sleep in code sections, to prevent ratelimiting.
PageSleep = 3 # Between every scraped page
ItemSleep = 2 # Between every scaped item
MailSleep = 9 # Between every sent mail

# Stop recursive navigation across posts pages if limit is reached. Set 0 for no limit (use with caution on new profiles with many posts).
MaxPagesRecursion = 10

# Whether or not to allow spaces in file names.
SpacesInFiles = True

### NOT IMPLEMENTED ###
# Whether or not to censor potentially private information in the logs, like email addresses (passwords are never printed despite of this setting).
#CensorLogs = True
### --------------- ###

# Max times to retry in case of an HTML error, before giving up.
MaxTryCount = 5

# Other advanced settings.
AppName = "bottocto-MastodonFeedHTML"
DbFile = "Database.txt"
UserAgent = AppName.replace('-','; ')
