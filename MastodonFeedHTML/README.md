Python program that scapes public posts from Mastodon profile URLs, and:  
 - Sends them to lists of email addresses;
 - Saves them as HTML files on your machine.

It works with any type of post, be it text or media.  
Retoots and replies are supported (and enabled by default).

It can theoretically see an unlimited number of posts and navigate recursively to see old ones.  
Limit is set to 10 pages by default (200 posts). If you set it to very high (or unlimited), you may need to set higher sleep times to prevent ratelimiting.

The program may be useful for archiving your own posts.  
Protip: you can repost your content on <https://blogger.com> via email with this tool!

## Install dependencies

```sh
pip3 install -r requirements.txt
```

## Configuration

Read `Example.Config.py`, copy it to `Config.py`, and edit it to your needs.
